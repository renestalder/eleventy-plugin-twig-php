# Contributing

- Please ensure you have Prettier installed for the code editor you use
  and that it consumes the config provided in this project.
- Use semantic commits according to the
  [common semantic commit recommenditions](https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716)

Apart from that, feel free to provide pull/merge requests to this
project.
