# 11ty (Eleventy) Twig PHP Plugin

> Under development

Plugin to add support for rendering Twig templates in Eleventy with the
original PHP Twig engine. Supports all features of the official Twig
specification, [version 2.x](https://twig.symfony.com/doc/2.x/).
**Requires PHP to be installed in the build environment**.

## Use cases

- You want build websites with 11ty and want to use the full feature set
  of Twig according to the supported version of this plugin.
- You want to use 11ty for building component libraries and
  documentations that are consumed by PHP projects (therefore need
  feature parity).
- You want to compile Twig outside of PHP projects.

## Alternatives

- **[Eleventy Plugin Twig](https://github.com/factorial-io/eleventy-plugin-twig)**:
  11ty Twig plugin using TwigJS under the hood. TwigJS doesn't have full
  feature parity with the original PHP engine. But the this plugin comes
  with handy additions on top for building websites, like image
  resizing, and might be a better, more lightweight approach to using
  Twig with 11ty if you have an environment without PHP support. Or
  simply want an out-of-the-box toolset for building websites with Twig.

## Getting started

### Requirements

- **PHP and composer needs to be available in your build environment**.
  This also means, these projects can only build in CI/CD environments
  that have PHP installed.

### Installation

Install the plugin via your preferred Node package manager.

_Example for npm:_

```
npm install --save-dev eleventy-plugin-twig-php
```

### Usage

Add the plugin to your Eleventy configuration, e.g. `.eleventy.js`:

```js
const twig = require('eleventy-plugin-twig-php');

module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(twig, options);

  // Other configuration...
};
```

#### Options

##### `twig.namespaces`

Set up Twig namespaces. Example:

```js
const twig = require('eleventy-plugin-twig-php');

module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(twig, {
    twig: {
      namespaces: {
        macros: 'path/to/macro/folder',
      },
    },
  });

  // Other configuration...
};
```

```twig
{# Imports path/to/macro/folder/my-macro.twig #}
{% from '@macros/my-macro' import my_macro %}
```

## Development

```sh
npm ci
npm run test
```

### Docker

The project comes with a `docker-compose.yml`. To make usage easier, it
also includes a `Makefile` to run node commands directly in the Docker
container:

```sh
make install
make test
```

## Acknowledgements

- The fantastic work of knapsack-cloud/basalt on the
  [PHP Twig renderer for node](https://github.com/knapsack-cloud/twig-renderer)
- [patternlab](https://patternlab.io/) for being a great toolset for
  years, that has shown me how the PHP version of Twig can be cleverly
  used together with Node.

## Contributing

See `CONTRIBUTING.md` for a few guidelines.
