/**
 * @param {import('./plugin').EleventyTwig.Options} options
 */
function plugin(eleventyConfig, options) {
  const extensionOptions = {
    outputFileExtension: 'html',
    init: async function () {
      const TwigRenderer = require('@basalt/twig-renderer');

      /**
       * @type {import('../types/twig-renderer-options').Options}
       */
      const twigRendererConfig = {
        src: {
          roots: [this.config.dir.input],
          namespaces: parseNamespaces(options?.twig?.namespaces, this.config.dir.input),
        },
      };

      // @ts-ignore
      const twigRenderer = new TwigRenderer(twigRendererConfig);
      this.config.twigRenderer = twigRenderer;
    },

    /**
     * @param {string} inputContent
     */
    compile: async function (inputContent) {
      /**
       * @param {Object} data full data cascade
       */
      return async (data) => {
        // Remove collections for now, see https://github.com/11ty/eleventy/issues/1237
        const { collections, ...filteredData } = data;
        return this.config.twigRenderer.renderString(inputContent, filteredData).then((compileResults) => {
          if (compileResults.ok) {
            return compileResults.html;
          } else {
            console.error(compileResults.message);
            return inputContent;
          }
        });
      };
    },
  };

  eleventyConfig.addExtension('twig', extensionOptions);
  eleventyConfig.addTemplateFormats('twig');
}

/**
 * @param {import('./plugin').EleventyTwig.Namespaces} namespaces
 * @param {string} root
 * @returns {import('../types/twig-renderer-options').Options['src']['namespaces']}
 */
function parseNamespaces(namespaces, root) {
  if (namespaces) {
    return Object.entries(namespaces).map(([alias, path]) => ({
      id: alias,
      recursive: true,
      paths: [`${root}/${path}`],
    }));
  }

  return [];
}

module.exports = plugin;
