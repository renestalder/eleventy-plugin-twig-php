export default function plugin(eleventyConfig: any, options: Options): void;

declare namespace EleventyTwig {
  export type Options = {
    twig: TwigOptions;
  };

  export type TwigOptions = {
    namespaces: Namespaces;
  };

  export type Namespaces = Record<string, string>;
}
