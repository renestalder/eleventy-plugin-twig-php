const twig = require('../lib/plugin');

module.exports = function (eleventyConfig) {
  /**
   * @type {import('../lib/plugin').EleventyTwig.Options}
   */
  const twigConfig = {
    twig: {
      namespaces: {
        macros: '_includes/macros',
      },
    },
  };

  eleventyConfig.addPlugin(twig, twigConfig);

  return {
    dataTemplateEngine: 'twig',
    markdownTemplateEngine: 'twig',
    templateFormats: ['twig', 'md'],
    dir: {
      input: 'examples',
      output: 'examples/dist',
    },
  };
};
