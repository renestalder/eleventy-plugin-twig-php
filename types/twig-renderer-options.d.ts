/**
 * A collection of PHP files and their functions to call that can alter the Twig_Environment right after it is created. This allows adding Twig Extensions and many others things.
 */
export type AlterTwigEnvironment = {
  /**
   * PHP file to include that contains the functions to call.
   */
  file?: string;

  /**
   * PHP Functions to execute with a param of Twig_Environment.
   */
  functions?: string[];
}[];

/**
 * If false, spins up PHP render server for each batch of render calls. If true, requires deliberate calls to start and stop render server.
 */
export type KeepRenderServerAlive = boolean;

export interface Options {
  src: TwigSourceFiles;

  /**
   * Path to directory that all paths in this config are relative from. Defaults to CWD.
   */
  relativeFrom?: string;
  alterTwigEnv?: AlterTwigEnvironment;

  /**
   * Should there be an 'info' key on the response object with extra debug details?
   */
  hasExtraInfoInResponses?: boolean;

  /**
   * Passed to creation of Twig Environment.
   */
  autoescape?: boolean;

  /**
   * Passed to creation of Twig Environment.
   */
  debug?: boolean;

  /**
   * Should the terminal output a lot of info?
   */
  verbose?: boolean;
  keepAlive?: KeepRenderServerAlive;

  /**
   * How many concurrent template rendering requests to do. Reduce if you get errors.
   */
  maxConcurrency?: number;
}

export interface TwigSourceFiles {
  /**
   * Root directories for Twig Loader
   */
  roots: string[];
  namespaces?: IndividualNamespaces[];
}

export interface IndividualNamespaces {
  /**
   * Machine Name of Namespace; will use as `@id/file.twig`.
   */
  id: string;

  /**
   * If set, will expand all paths to include all sub-directories.
   */
  recursive?: boolean;

  /**
   * Paths to directories to look for twig files in under this Namespace.
   */
  paths: string[];
}
