install:
	docker compose run --rm php-node bash -c "npm ci --cache .npm --prefer-offline"

test:
	docker compose run --rm php-node bash -c "npm run test"